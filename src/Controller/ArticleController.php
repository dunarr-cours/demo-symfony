<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/article', name: 'app_article')]
    public function index(ArticleRepository $articleRepository, CategoryRepository $categoryRepository): Response
    {
        /*$monArticle = new Article();
        $monArticle->setTitle("Mon premier article");
        $monArticle->setContent("Lorem ipsum dolor sit amet");
        $monArticle->setPublishedAt(new DateTimeImmutable());

        $articleRepository->add($monArticle);

        dump($monArticle);*/

        $monArticle = $articleRepository->find(1);
        $maCategorie = $categoryRepository->find(1);
        $monArticle->setCategory($maCategorie);
        $articleRepository->add($monArticle);




        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
        ]);
    }

    #[Route("/liste-articles", name: "liste_articles")]
    public function listArticles(ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findAll();
        return $this->render("article/liste.html.twig", [
            "articles" => $articles
        ]);
    }

    /*#[Route("/show-article/{id}", name: "show_article")]
    public function showArticle(ArticleRepository $articleRepository, $id)
    {
        $article = $articleRepository->find($id);
        return $this->render("article/show.html.twig", [
            "article" => $article
        ]);
    }*/

    #[Route("/show-article/{id}", name: "show_article")]
    public function showArticle(Article $article)
    {
        return $this->render("article/show.html.twig", [
            "article" => $article
        ]);
    }

    #[Route("/create-article", name: "create_article")]
    public function createArticle(Request $request, ArticleRepository $repo)
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($article);
        }

        return $this->render("article/create.html.twig", [
            "articleform" => $form->createView()
        ]);
    }

    #[Route("/edit-article/{id}", name: "edit_article")]
    public function updateArticle(Request $request, ArticleRepository $repo, Article $article)
    {


        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($article);
        }

        return $this->render("article/create.html.twig", [
            "articleform" => $form->createView()
        ]);
    }

    #[Route('/delete/{id}')]
    public function deleteArticle(Article $article, ArticleRepository $repo)
    {
        $repo->remove($article);
        return $this->redirectToRoute("liste_articles");
    }
}
